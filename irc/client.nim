import asyncdispatch, asyncnet
import strutils
import events

export asyncdispatch

type
    IrcUser* = object
        nick*: string
        user*: string
        host*: string
        realname*: string

proc newUser*(host: string): IrcUser =
    let s = strutils.split(host, {'!','@'})
    return IrcUser(nick: s[0], user: s[1], host: s[2])

proc hostmask*(user: IrcUser): string =
    return user.nick & "!" & user.user & "@" & user.host

proc newUser*(nick: string, user: string, realname: string): IrcUser =
    return IrcUser(nick: nick, user: user, realname: realname)

type
    Client* = ref object
        address*: string
        port*: int
        user*: IrcUser
        password*: string
        socket: AsyncSocket

proc newClient*(address: string, port: int, user: IrcUser): Client =
    result = Client(address: address, port: port, user: user)

proc connect*(client: Client) {.async.} =
    client.socket = newAsyncSocket()
    await client.socket.connect(client.address, Port(client.port))
